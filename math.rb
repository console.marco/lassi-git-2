#! /usr/bin/ruby



puts "Inserisci due numeri"
n1 = gets
n2 = gets

scelta = ""
while (not(scelta.eql? "S") and not(scelta.eql? "M"))
	puts "Inserisci S per somma e M per moltiplicazione"
	scelta = gets.chomp
end

if(scelta == "S") 
	risultato = Integer(n1) + Integer(n2)
end

if(scelta == "M") 
	risultato = Integer(n1) * Integer(n2)
end

puts "Il risultato è " + String(risultato)